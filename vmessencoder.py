import base64
import json

def encode(data: dict) -> str:
  # Convert dictionary to JSON string
  json_string = json.dumps(data)

  # Encode JSON string
  json_bytes = json_string.encode('utf-8')
  base64_bytes = base64.b64encode(json_bytes)
  base64_string = base64_bytes.decode('utf-8')

  return 'vmess://'+base64_string

# Ceno

[![pipeline status](https://gitlab.com/farhansa/ceno/badges/master/pipeline.svg)](https://gitlab.com/farhansa/ceno/commits/master)
[![coverage report](https://gitlab.com/farhansa/ceno/badges/master/coverage.svg)](https://gitlab.com/farhansa/ceno/commits/master)

Ceno is a Telegram bot that allows users to create and manage virtual private network (VPN) connections.

## Requirements

- Docker
- GitLab CI/CD

## Installation

1. Clone the repository:

```bash
git clone https://gitlab.com/farhansa/ceno.git
```


2. Build the Docker image:

```bash
docker build -t ceno:latest .
```

## Usage

To use the Ceno bot, you can either run it locally using the Docker image or deploy it to a remote server using GitLab CI/CD.

### Running locally

To run the Ceno bot locally using the Docker image, use the following command:

```bash
docker run -it ceno:latest
```

### Deploying to a remote server
To deploy the Ceno bot to a remote server using GitLab CI/CD, you can push your changes to the GitLab repository. This will trigger the pipeline defined in the .gitlab-ci.yml file, which will build the Docker image and deploy it to the remote server.

### Dockerfile
The Dockerfile included in this repository can be used to build a Docker image for the Ceno bot. The Docker image is based on the python:3.9-slim-buster base image and installs the necessary dependencies for running the bot.

To build the Docker image, run the following command in the root directory of the project:
```bash
docker build -t <image-name>:<tag> .
```
Replace <image-name> and <tag> with the desired name and tag for the image. For example, to build an image named ceno with the latest tag, you can use the following command:

```bash
docker build -t ceno:latest .
```

### .gitlab-ci.yml
The .gitlab-ci.yml file included in this repository can be used to automate the build and deployment of the Ceno bot using GitLab CI/CD.

The pipeline defined in the .gitlab-ci.yml file has two stages: build and deploy.


### Build stage
The build stage builds a Docker image for the Ceno bot and pushes it to the GitLab Container Registry.

To authenticate to the GitLab Container Registry, the pipeline uses the gitlab-ci-token placeholder as the username and the GITLAB_PRIVATE_TOKEN secret variable as the password. The GITLAB_PRIVATE_TOKEN variable should be added as a secret variable in the CI/CD settings.

To build the Docker image and push it to the GitLab Container Registry, run the following command in the root directory of the project:

```bash
docker build -t registry.gitlab.com/<namespace>/<project>:latest .
docker push registry.gitlab.com/<namespace>/<project>:latest
```
Replace `<namespace>` and `<project>` with the namespace and project name in your GitLab repository.

### Deploy stage
The deploy stage authenticates to the remote server using an SSH key and deploys the Ceno bot using the Docker image pushed to the GitLab Container Registry in the build stage.

To authenticate to the remote server, the pipeline uses the SSH_KEY secret variable as the private key for the ssh command. The SSH_KEY variable should be added as a secret variable in the CI/CD settings.

The deploy stage authenticates to the remote server using an SSH key and deploys the Ceno bot using the Docker image pushed to the GitLab Container Registry in the build stage.

To authenticate to the remote server, the pipeline uses the SSH_KEY secret variable as the private key for the ssh command. The SSH_KEY variable should be added as a secret variable in the CI/CD settings.

Replace `<server-ip>` with the IP address of the remote server, and `<namespace>` and `<project>` with the namespace and project name in your GitLab repository.

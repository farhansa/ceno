import telebot
from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton
from main import create_account
import database
from dotenv import load_dotenv
import os
import json
load_dotenv()




TELEGRAM_TOKEN = os.environ['TELEGRAM_TOKEN']
bot = telebot.TeleBot(TELEGRAM_TOKEN)

def read_card_number():
    with open('card_number.json', 'r') as file:
        card_number = json.load(file)
    return card_number

def handle_setting(message):
    if message.from_user.id == 111810082:
        card_number = message.text
        with open('card_number.json', 'w') as file:
            json.dump(card_number, file)
        bot.send_message(message.chat.id, f'Card number updated to {card_number}')

@bot.message_handler(commands=['setting'])
def setting_command(message):
    bot.send_message(message.chat.id, 'Please enter the new card number')
    bot.register_next_step_handler(message, handle_setting)

def send_saved_photos(user_id , phone_type):
    # List of photos to send
    photos= []
    if phone_type == 'IOS':
        photos = ["ios1.jpg","ios2.jpg","ios3.jpg","ios4.jpg","ios5.jpg","ios6.jpg","ios7.jpg",]
    for photo in photos:
        # Send the photo to the user
        bot.send_photo(user_id, photo=open(f'photos/{photo}', "rb"))


def send_status(chat_id):
    response = database.retrieve_data(str(chat_id))
    if response is not None:
        # Initialize the message string
        message = ""
        
        # Loop through the output list
        for item in response:
            # Add the name and expiry time to the message string
            message += f"`{item['vmess_config']}`\n\ndays remaining: {item['days_remaining']}\n\n"
        
        # Return the message string
        bot.send_message(chat_id, message ,parse_mode="markdown")
    else:
        bot.send_message(chat_id, "You don't have any VPN connections.")

def send_welcome_message(chat_id):
    bot.send_message(chat_id, "Thank you for your payment. Welcome to our service!")

def send_payment_request(chat_id):
    bot.send_message(chat_id, "لطفا عکس رسید واریز را ارسال کنید.")

def send_payment_rejected(chat_id):
    bot.send_message(chat_id, "Sorry, your payment was rejected. Please contact us for more information.")

@bot.message_handler(content_types=['photo'])
def handle_photo(message):
    user_id = message.chat.id  # Get the ID of the user who sent the photo
    bot.forward_message(111810082, user_id, message.message_id)  # Forward the photo to another chat
    markup = InlineKeyboardMarkup()
    markup.row_width = 2
    markup.add(InlineKeyboardButton("Confirm", callback_data=f"confirm_{user_id}"),
               InlineKeyboardButton("Reject", callback_data=f"reject_{user_id}"))  # Add "Confirm" and "Reject" buttons with user ID as callback data
    bot.send_message(111810082, "Please confirm or reject the payment by clicking the button below:", reply_markup=markup)

    # Send a message to the user asking them to wait for their payment to be confirmed
    bot.send_message(user_id, "Thank you for your payment. We are currently reviewing your payment. Please wait for a confirmation message.")

@bot.callback_query_handler(func=lambda call: True)
def callback_query(call):
    if call.data.startswith("confirm_"):  # Check if the callback data starts with "confirm_"
        user_id = call.data.split("_")[1]  # Extract the user ID from the callback data
        send_welcome_message(user_id)  # Send the welcome message to the user who sent the photo
        markup = InlineKeyboardMarkup()
        bot.edit_message_reply_markup(111810082, call.message.message_id, reply_markup=markup)   # Remove the "Confirm" and "Reject" buttons and send a message to the admin
        bot.send_message(111810082, "Payment confirmed and welcome message sent to user.")
        response = create_account(str(user_id)) # Create
        markup.add(
            InlineKeyboardButton("Android", callback_data=f"Android_{user_id}"),
            InlineKeyboardButton("IOS", callback_data=f"IOS_{user_id}")
            ) 
        bot.send_message(user_id, '`'+response['vmess_config']+'`', parse_mode="markdown" , reply_markup=markup)       
    elif call.data.startswith("reject_"):  # Check if the callback data starts with "reject_"
        user_id = call.data.split("_")[1]  # Extract the user ID from the callback data
        send_payment_rejected(user_id)  # Send the payment rejected message to the user who sent the photo

        # Remove the "Confirm" and "Reject" buttons and send a message to the admin
        markup = InlineKeyboardMarkup()
        bot.edit_message_reply_markup(111810082, call.message.message_id, reply_markup=markup)
        bot.send_message(111810082, "Payment rejected and notification sent to user.")
    elif call.data == "cb_yes":
        card_number = read_card_number()
        bot.send_message(call.message.chat.id, f"لطفا مبلغ ۲۵۰.۰۰۰ تومان به کارت `{card_number}` واریز کنید.", parse_mode="markdown")
        send_payment_request(call.message.chat.id)
    elif call.data == "cb_no":
        send_status(call.message.chat.id)
        # bot.answer_callback_query(call.id, "Answer is No")
    elif call.data == "cb_con":
        bot.answer_callback_query(call.id, "* ماهیانه ۲۵۰.۰۰۰ تومان\n* قابل استفاده :Android, IOS, Windows, Mac Linux\n" , show_alert=True)
    elif call.data.startswith("IOS_"):  # Check if the callback data starts with "reject_"
        user_id = call.data.split("_")[1]  # Extract the user ID from the callback data
        send_saved_photos(user_id, phone_type='IOS')


def gen_markup():
    markup = InlineKeyboardMarkup()
    markup.row_width = 2
    markup.add(
        InlineKeyboardButton("خرید VPN", callback_data="cb_yes"),
        InlineKeyboardButton("وضعیت", callback_data="cb_no"),
        InlineKeyboardButton("شرایط", callback_data="cb_con"))
    return markup

@bot.message_handler(func=lambda message: True)
def message_handler(message):
    string= '''
        لطفا انتخاب کنید.
    '''
    bot.send_message(message.chat.id, string, reply_markup=gen_markup())
bot.infinity_polling()


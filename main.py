import base64
import datetime
import json
import random
from re import X
import uuid
from typing import Union
import requests
from fastapi import FastAPI
import vmessencoder
import iptables
import database

from dotenv import load_dotenv
import os
load_dotenv()

IRAN_SERVER_IP = os.environ['IRAN_SERVER_IP']
XUI_USERNAME = os.environ['XUI_USERNAME']
XUI_PASSWORD = os.environ['XUI_PASSWORD']
XUI_PORT = os.environ['XUI_PORT']


app =FastAPI()

database.create_table()


def create_timestamp():

    # Get the current date and time
    now = datetime.datetime.now()

    # Add one month to the current date and time
    later = now + datetime.timedelta(weeks=4)

    # Get the timestamp for the current date and time
    timestamp_now = now.timestamp()
    # print(timestamp_now)

    # Get the timestamp for the date and time one month later
    timestamp_later = later.timestamp()*1000
    return int(timestamp_later)

def create_uuid() -> str:
    return str(uuid.uuid4())
    
def create_random_port() -> int:
    return random.randint(1000,65536)

def encode_base64(name):
    bytes_obj = name.encode('utf-8')
    encoded_bytes = base64.b64encode(bytes_obj)
    encoded_string = encoded_bytes.decode('utf-8')
    return encoded_string

def login() -> Union[str, None]: 

    headers = {
        'Host': f'{IRAN_SERVER_IP}:{XUI_PORT}',
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0',
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'en-US,en;q=0.5',
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': f'http://{IRAN_SERVER_IP}:{XUI_PORT}',
        'Connection': 'close',
        'Referer': f'http://{IRAN_SERVER_IP}:{XUI_PORT}/',
    }

    data = {
        'username': f'{XUI_USERNAME}',
        'password': f'{XUI_PASSWORD}',
    }

    response = requests.post(f'http://{IRAN_SERVER_IP}:{XUI_PORT}/login', headers=headers, data=data, verify=False)
    res = response.headers.get('Set-Cookie')
    if res:
        return res.split(';')[0]
    else:
        return None

def create_account(name: str):
    random_port = create_random_port()
    login_var = login()
    session = login_var.split('=')[1]
    encoded_name = encode_base64(name)
    uuid_random = create_uuid()
    expiryTime = create_timestamp()

    cookies = {
        'session': f'{session}',
    }

    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0',
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'en-US,en;q=0.5',
        # 'Accept-Encoding': 'gzip, deflate',
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': f'http://{IRAN_SERVER_IP}:{XUI_PORT}',
        'Connection': 'keep-alive',
        'Referer': f'http://{IRAN_SERVER_IP}:{XUI_PORT}/xui/inbounds',
        'Cookie': f'{login_var}',
    }

    data = {
        'up': '0',
        'down': '0',
        'total': '0',
        'remark': f'{encoded_name}',
        'enable': 'true',
        'expiryTime': f'{expiryTime}',
        'listen': '',
        'port': f'{random_port}',
        'protocol': 'vmess',
        f'settings': f'{{\n  "clients": [\n    {{\n      "id": "{uuid_random}",\n      "alterId": 0\n    }}\n  ],\n  "disableInsecureEncryption": false\n}}',
        'streamSettings': '{\n  "network": "ws",\n  "security": "none",\n  "wsSettings": {\n    "path": "/",\n    "headers": {}\n  }\n}',
        'sniffing': '{\n  "enabled": true,\n  "destOverride": [\n    "http",\n    "tls"\n  ]\n}',
    }
    response = requests.post(f'http://{IRAN_SERVER_IP}:{XUI_PORT}/xui/inbound/add', cookies=cookies, headers=headers, data=data)
    if json.loads(response.text)['success']:
        vmess_config = {
            "v": "2",
            "ps": f"{encoded_name}",
            "add": f"{IRAN_SERVER_IP}",
            "port": int(random_port),
            "id": f"{uuid_random}",
            "aid": 0,
            "net": "ws",
            "type": "none",
            "host": "",
            "path": "/",
            "tls": "none"
            }
        vmess_config = vmessencoder.encode(vmess_config)
        iptables.setup(random_port)
        database.insert_data(name, vmess_config, expiryTime)
    else:
        vmess_config = None
    return {'response': response.text, 'vmess_config': vmess_config}
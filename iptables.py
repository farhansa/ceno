import paramiko
import time
from dotenv import load_dotenv
import os
load_dotenv()

IRAN_SERVER_IP = os.environ['IRAN_SERVER_IP']
FOREIGN_SERVER_IP = os.environ['FOREIGN_SERVER_IP']
IRAN_SERVER_USERNAME = os.environ['IRAN_SERVER_USERNAME']
IRAN_SERVER_PASSWORD = os.environ['IRAN_SERVER_PASSWORD']



def setup(port):
    # Create an SSH client
    ssh = paramiko.SSHClient()

    # Set missing host key policy to AutoAddPolicy
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    # Connect to the server
    ssh.connect(hostname = IRAN_SERVER_IP ,username = IRAN_SERVER_USERNAME,password = IRAN_SERVER_PASSWORD)

    # Execute the command
    stdin, stdout, stderr = ssh.exec_command(
        f'sudo iptables -t nat -I PREROUTING -p tcp -d {IRAN_SERVER_IP} --dport {port} -j DNAT --to-destination {FOREIGN_SERVER_IP}:{port}')
    time.sleep(1)

    # Print the command output
    print(stdout.read())

    # Close the connection
    ssh.close()

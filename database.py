import sqlite3
import utils
def create_table():
    # Connect to the database
    conn = sqlite3.connect('mydatabase.db')

    # Create a cursor
    cursor = conn.cursor()

    # Check if the table exists
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='users'")
    result = cursor.fetchone()

    # If the table does not exist, create it
    if not result:
        cursor.execute("CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, vmess_config TEXT, expiryTime INTEGER)")

    # Commit the transaction
    conn.commit()

    # Close the connection
    conn.close()

def insert_data(name: str, vmess_config: str, expiryTime: int):
    # Connect to the database
    conn = sqlite3.connect('mydatabase.db')

    # Create a cursor
    cursor = conn.cursor()

    # Insert data into the table
    cursor.execute("INSERT INTO users (name, vmess_config, expiryTime) VALUES (?, ?, ?)", (name, vmess_config, expiryTime))

    # Commit the transaction
    conn.commit()

    # Close the connection
    conn.close()


def retrieve_data(name: str):
    # Connect to the database
    conn = sqlite3.connect('mydatabase.db')

    # Create a cursor
    cursor = conn.cursor()

    # Retrieve data from the table
    cursor.execute("SELECT * FROM users WHERE name=?", (name,))
    results = cursor.fetchall()

    # Close the connection
    conn.close()

    if results:
        # Return the data as a list of dictionaries
        return [{
            "id": row[0],
            "name": row[1],
            "vmess_config": row[2],
            "days_remaining": utils.days_until_timestamp(row[3])
        } for row in results]
    else:
        # Return a message if the data does not exist
        return None

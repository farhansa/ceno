from datetime import datetime


def days_until_timestamp(timestamp: str) -> int:
    # Convert the timestamp from milliseconds to seconds
    timestamp_in_seconds = int(timestamp) // 1000

    # Parse the timestamp string into a datetime object
    timestamp_datetime = datetime.fromtimestamp(timestamp_in_seconds)

    # Get the current datetime
    current_datetime = datetime.now()

    # Calculate the difference between the current datetime and the timestamp datetime
    diff = timestamp_datetime - current_datetime

    # Return the number of days remaining (rounded down to the nearest whole number)
    return diff.days